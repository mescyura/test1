import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int choice;
        do {
            System.out.println("Welcome to url shortener");
            System.out.println("What do you want?");
            System.out.println("1. Save URL.");
            System.out.println("2. Get URL.");
            System.out.println("3. Exit.");
            choice = scanner.nextInt();
        } while (choice != 3);
        System.out.println("Bye!");
    }
}
